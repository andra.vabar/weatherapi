<?php

$url = 'https://api.openweathermap.org/data/2.5/weather?lat=58.2550&lon=22.4919&appid=be23ed97467a3f0d12df78d3b2dd0a7b&units=metric';
$cacheFile = './cache.json';
$cacheTime = 60;

if ( file_exists($cacheFile) && (time() - filemtime($cacheFile) < $cacheTime ) ) {
    $content = file_get_contents($cacheFile);   
} else {
    $content = file_get_contents($url);

    $file = fopen($cacheFile, 'w');
    fwrite($file, $content);
    fclose($file);
}

$data = json_decode($content);


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ilm PHPga</title>
</head>
<body>
    <h1>Current Weather</h1>
    <p>Location: <?= $data->name; ?></p>
    <p>Temperature now (°C): <?= $data->main->temp; ?></p>
    <p>Feels like (°C): <?= $data->main->feels_like; ?></p>
    <p>Weather description: <?= $data->weather[0]->description; ?></p>
    <img src="http://openweathermap.org/img/wn/<?= $data->weather[0]->icon; ?>@2x.png" alt="">
</body>
</html>