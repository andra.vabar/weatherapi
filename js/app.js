const locationCell = document.getElementById('location');
const tempCell = document.getElementById('temp');
const maxtempCell = document.getElementById('maxtemp');
const mintempCell = document.getElementById('mintemp');
const descriptionCell = document.getElementById('description');
const iconCell = document.getElementById('icon');

fetch('https://api.openweathermap.org/data/2.5/weather?lat=58.2550&lon=22.4919&appid=be23ed97467a3f0d12df78d3b2dd0a7b&units=metric')
  .then(response => response.json())
  .then(data =>  {
        console.log(data);
        locationCell.innerText = data.name;
        tempCell.innerText = data.main.temp;
        maxtempCell.innerText = data.main.temp_max;
        mintempCell.innerText = data.main.temp_min;
        descriptionCell.innerText = data.weather[0].main;
  

        const iconImg = document.createElement('img');
        iconImg.src = `http://openweathermap.org/img/wn/${data.weather[0].icon}.png`;
        iconCell.append(iconImg);
    
    });